package main;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Ejecutable {
    public static final String TIME_ZONE="Z";

    public static void main(String[] args){
        System.out.println("Prueba de fechas");
        String fecha = "01042023";
        long resultado = 0;
//        resultado = CalculateDiffOnDays(fecha);
        System.out.println("La diferencia en días con la función 1 es " + resultado);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("ddMMyyyy");
        String today = simpleDateFormat.format(new Date());
        System.out.println("Hoy es " + today);

        resultado = givenTwoDatesBeforeJava8(fecha);
        System.out.println("La diferencia en días con la función 2 es " + resultado);
    }

    public static int CalculateDiffOnDays(String fecha){
        int daysDiff = 0;
        try {
            Date fromFile = new Date( fecha);
            Date today = new Date();
            long diff = fromFile.getTime() - today.getTime();
            daysDiff = (int) (diff / (1000 * 60 * 60* 24));
        } catch (NullPointerException ex) {
            System.err.println("Fecha nula " + fecha);
        }
        return daysDiff;
    }

    public static long givenTwoDatesBeforeJava8(String fechaDesde) {
        long diff=0L;
        try{
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
        Date firstDate = sdf.parse(fechaDesde);
        String fechaHasta = sdf.format(new Date());
        Date secondDate = sdf.parse(fechaHasta);
        long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
            diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
        } catch(ParseException error){
            System.err.println("Error on Rules Astrazeneca 944_123: Calculate diff on days. ");
            System.err.println(error.getMessage());
        }
        return diff;
    }

    /**
     * @param inboundDate Type String.
     * @param maskIn	Type String.
     * @param maskOut	Type String.
     * @return Return a string with date.
     * @throws ParseException
     */
    static public String maskDate(String inboundDate, String maskIn, String maskOut) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat(maskIn);
        String dateValid = validateDate(inboundDate);
        Date date = formatter.parse(dateValid);
        formatter = new SimpleDateFormat(maskOut);
        return formatter.format(date);
    }

    /**
     * @param date Type String.
     * @return Return a string with date.
     **/
    private static String validateDate(String date){
        String outDate = "";
        if ( date.contains(TIME_ZONE) )
            outDate = date.replace(TIME_ZONE, "");
        else
            outDate = date;

        return outDate;
    }

}
